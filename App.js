import React, {useState} from "react";
import {StyleSheet, View} from 'react-native';
import Header from "./components/header";
import StartScreen from "./screens/StartScreen";
import GameScreen from "./screens/GameScreen";
import GameResults from "./screens/GameResults";

export default function App() {
    const [userNumber, setUserNumber] = useState();
    const [computerNumber, setComputerNumber] = useState();
    const [winner, setWinner] = useState('');

    const restartGameHandler = () => {
        setUserNumber(null);
        setComputerNumber(null);
        setWinner('')
    }

    const startGameHandler = selectedNumber => {
        setUserNumber(selectedNumber);
        setComputerNumber(null);
        setWinner('');
    }

    const winnerHandler = winner => {
        setWinner(winner);
    }

    let content = <StartScreen onStartGame={startGameHandler} />;

    if (userNumber && winner === '') {
        content = <GameScreen userChoice={userNumber} computerChoice={computerNumber} onResetGame={restartGameHandler}
                              onEndOfGame={winnerHandler}/>;
    } else if (winner !== '') {
        content = <GameResults whoWon={winner} onPlayAgain={restartGameHandler} />;
    }

    return (
        <View style={styles.screen}>
            <Header title="Odds or Evens"/>
            {content}
        </View>
    );
}

const styles = StyleSheet.create({
    screen: {
        flex: 1
    }
});
