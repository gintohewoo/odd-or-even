import React from "react";
import {View, Text, StyleSheet} from "react-native";
import Colors from "../constatns/colors";

const NumberContainer = props => {
    return (
        <View style={styles.container}>
            <Text style={styles.number}>{props.children}</Text>
        </View>
    )
};

const styles = StyleSheet.create({
    container: {
        borderWidth: 2,
        borderColor: Colors.primary,
        padding: 20,
        borderRadius: 20,
        marginVertical: 15,
        alignItems: "center",
        justifyContent: "center"
    },
    number: {
        color: 'black',
        fontSize: 24,
        fontWeight: "bold"
    }
});

export default NumberContainer;
