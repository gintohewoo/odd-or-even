import React from "react";
import { StyleSheet, View, Text } from "react-native";
import Colors from "../constatns/colors";

const Header = props => {
    return (
        <View style={styles.header}>
            <Text style={styles.headerText}>{props.title}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    header: {
        width: "100%",
        height: 90,
        paddingTop: 36,
        backgroundColor: Colors.primary,
        alignItems: 'center',
        justifyContent: 'center'
    },
    headerText: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 18
    }
});

export default Header;
