import React from "react";
import { View, Text, Button, StyleSheet} from 'react-native';
import Colors from "../constatns/colors";

const GameResults = props => {
    return (
        <View style={styles.screen}>
            <Text style={styles.resultsText}>{props.whoWon} Won!</Text>
            <Text style={styles.resultsText}>Good Game.</Text>
            <View style={styles.buttonContainer}>
                <Button title="Play Again?" onPress={props.onPlayAgain} color={Colors.primary} />
            </View>
        </View>
    )
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    resultsText: {
        fontSize: 22,
        fontWeight: "bold",
        marginVertical: 10
    },
    buttonContainer: {
        marginVertical: 45
    }
});

export default GameResults;
