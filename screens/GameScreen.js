import React, {useState, useEffect} from "react";
import {View, Text, StyleSheet, Button} from 'react-native';
import NumberContainer from "../components/NumberContainer";
import Card from "../components/Card";
import Colors from "../constatns/colors";

const generateComputerNumber = (min, max, exclude) => {
    min = Math.ceil(min);
    max = Math.floor(max);
    const randomNumber = Math.floor(Math.random() * (max - min) + min);
    if (randomNumber === exclude) {
        return generateComputerNumber(min, max, exclude);
    } else {

        return randomNumber;
    }
};

const GameScreen = props => {
    const [computerNumber, setComputerNumber] = useState(generateComputerNumber(1, 100, props.userChoice));

    const { userChoice, onEndOfGame, computerChoice } = props;

    const evaluateGame = () => {
        if ((computerNumber + userChoice) % 2 === 1) {
            onEndOfGame("I");
        } else {
            onEndOfGame("You");
        }
    }

    return (
        <View style={styles.screen}>
            <Text>Computer choice:</Text>
            <NumberContainer>{computerNumber}</NumberContainer>
            <Card style={styles.buttonContainer}>
                <Button title="Who won?" onPress={evaluateGame} color={Colors.primary} />
                <Button title="Restart" onPress={props.onResetGame} color={Colors.accent}/>
            </Card>
        </View>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        padding: 20,
        alignItems: "center"
    },
    buttonContainer: {
        flexDirection: "row",
        justifyContent: 'space-around',
        marginVertical: 20,
        width: 320,
        maxWidth: '80%'
    }
});

export default GameScreen;
