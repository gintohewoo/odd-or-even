import React, { useState } from "react";
import {View, Text, StyleSheet, Button, TouchableWithoutFeedback, Keyboard, Alert } from "react-native";
import Card from "../components/Card";
import Colors from "../constatns/colors";
import Input from "../components/Input";
import NumberContainer from "../components/NumberContainer";

const StartScreen = props => {

    const [enteredValue, setEnteredValue] = useState('');
    const [confirmed, setConfirmed] = useState(false);
    const [numberToUse, setNumberToUse] = useState();

    const numberInputHandler = inputText => {
        setEnteredValue(inputText.replace(/[^0-9]/g, ''));
    };

    const resetInputHandler = () => {
        setEnteredValue('');
        setConfirmed(false);
    }

    const confirmInputHandler = () => {
        const selectedNumber = parseInt(enteredValue);
        if (isNaN(selectedNumber) || selectedNumber < 1 || selectedNumber > 99) {
            Alert.alert('Invalid input!', 'Input must be between 0 and 100.', [{text: 'Ok', style: 'destructive', onPress: resetInputHandler}] )
            return;
        }
        setConfirmed(true);
        setNumberToUse(selectedNumber);
        setEnteredValue('');
        Keyboard.dismiss();
    }

    let confirmedOutput;

    if (confirmed) {
        confirmedOutput = (
            <Card style={styles.playerNumber}>
                <Text>Chosen Number: </Text>
                    <NumberContainer>{numberToUse}</NumberContainer>
                <Button title="Go" onPress={() => props.onStartGame(numberToUse)} color={Colors.primary} />
            </Card>
        );
    }

    return (
        <TouchableWithoutFeedback onPress={() => {
            Keyboard.dismiss();
        }}>
        <View style={styles.screen}>
            <Text style={styles.title}>Welcome to a game of odds and evens, where I am odds and you are evens!</Text>
            <Card style={styles.inputContainer}>
                <Text style={styles.title}>Select a number:</Text>
                <Input
                    style={styles.input}
                    blurOnSubmit
                    autoCorrect={false}
                    keyboardType="number-pad"
                    maxLength={2}
                    onChangeText={numberInputHandler}
                    value={enteredValue}
                />
                <View style={styles.buttonContainer}>
                    <View style={styles.button}><Button title="Confirm" onPress={confirmInputHandler} color={Colors.primary} /></View>
                    <View style={styles.button}><Button title="Reset" onPress={resetInputHandler} color={Colors.accent} /></View>
                </View>
            </Card>
            {confirmedOutput}
        </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
    screen: {
        flex: 1,
        padding: 10,
        alignItems: "center"
    },
    title: {
        fontSize: 20,
        marginVertical: 20,
    },
    inputContainer: {
        width: 300,
        maxWidth: "80%",
        alignItems: "center"
    },
    buttonContainer: {
        flexDirection: "row",
        width: "100%",
        justifyContent: 'space-between',
        paddingHorizontal: 15
    },
    button: {
        width: 100
    },
    input: {
        width: 50,
        textAlign: "center",
        marginBottom: 25
    },
    playerNumber: {
        marginTop: 20,
        alignItems: "center"
    }
});

export default StartScreen;
